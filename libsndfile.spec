Name: libsndfile
Version: 1.0.28
Release: 18
Summary: Library for reading and writing sound files
License: LGPLv2+ and GPLv2+ and BSD
URL: http://www.mega-nerd.com/libsndfile/
Source0: http://www.mega-nerd.com/libsndfile/files/libsndfile-%{version}.tar.gz

BuildRequires: alsa-lib-devel gcc gcc-c++ flac-devel
BuildRequires: libogg-devel libtool libvorbis-devel pkgconfig
BuildRequires: sqlite-devel

Patch0:		libsndfile-1.0.25-zerodivfix.patch
Patch1:		libsndfile-1.0.28-flacbufovfl.patch
Patch2:		libsndfile-1.0.29-cve2017_6892.patch
Patch3:		libsndfile-1.0.28-cve2017_12562.patch
Patch6000:	libsndfile-1.0.28-CVE-2018-13139-CVE-2018-19432.patch
Patch6001:	libsndfile-1.0.28-src-wav.c-Fix-WAV-Sampler-Chunk-tune-parsing.patch
Patch6002:	libsndfile-1.0.28-CVE-2018-19758.patch
Patch6003:	libsndfile-1.0.28-CVE-2019-3832.patch
Patch6004:	libsndfile-1.0.28-CVE-2017-17456-CVE-2017-17457-CVE-2018-19661-CVE-2018-19662.patch
Patch6005:	libsndfile-1.0.28-CVE-2017-14634.patch
Patch6006:	libsndfile-1.0.28-CVE-2017-8362.patch

%description
Libsndfile is a C library for reading and writing files containing
sampled sound such as MS Windows WAV and the Apple/SGI AIFF format
through one standard library interface.

%package devel
Summary: Development package for libsndfile
Requires: %{name}%{?_isa} = %{version}-%{release} pkgconfig

%description devel
The development package for libsndfile.

%package utils
Summary: command line utilities for libsndfile
Requires: %{name} = %{version}-%{release}

%description utils
The command line utilities for libsndfile.

%package utils-help
Summary: Help files for %{name}-utils
BuildArch: noarch
%description utils-help
Help files for %{name}-utils.

%prep
%autosetup -n %{name}-%{version} -p1

%build
%configure \
	--disable-dependency-tracking \
	--enable-sqlite \
	--enable-alsa \
	--enable-largefile \
	--disable-static
%disable_rpath
%make_build

%install
%make_install
rm -rf __docs
mkdir __docs
cp -pR $RPM_BUILD_ROOT%{_docdir}/%{name}/* __docs
rm -rf $RPM_BUILD_ROOT%{_docdir}/%{name}
mv %{buildroot}%{_includedir}/sndfile.h \
   %{buildroot}%{_includedir}/sndfile-%{__isa_bits}.h

cat > %{buildroot}%{_includedir}/sndfile.h <<EOF
#include <bits/wordsize.h>

#if __WORDSIZE == 32
# include "sndfile-32.h"
#elif __WORDSIZE == 64
# include "sndfile-64.h"
#else
# error "unexpected value for __WORDSIZE macro"
#endif
EOF

%check
LD_LIBRARY_PATH=$PWD/src/.libs make check

%ldconfig_scriptlets

%files
%{_libdir}/%{name}.so.*
%doc AUTHORS README NEWS
%license COPYING

%files utils
%{_bindir}/sndfile-cmp
%{_bindir}/sndfile-concat
%{_bindir}/sndfile-convert
%{_bindir}/sndfile-deinterleave
%{_bindir}/sndfile-info
%{_bindir}/sndfile-interleave
%{_bindir}/sndfile-metadata-get
%{_bindir}/sndfile-metadata-set
%{_bindir}/sndfile-play
%{_bindir}/sndfile-regtest
%{_bindir}/sndfile-salvage
%exclude %{_bindir}/sndfile-jackplay

%files devel
%{_includedir}/sndfile.h
%{_includedir}/sndfile.hh
%{_includedir}/sndfile-%{__isa_bits}.h
%{_libdir}/%{name}.so
%{_libdir}/pkgconfig/sndfile.pc
%doc __docs ChangeLog
%exclude %{_libdir}/*.la

%files utils-help
%{_mandir}/man1/sndfile-cmp.1*
%{_mandir}/man1/sndfile-concat.1*
%{_mandir}/man1/sndfile-convert.1*
%{_mandir}/man1/sndfile-deinterleave.1*
%{_mandir}/man1/sndfile-info.1*
%{_mandir}/man1/sndfile-interleave.1*
%{_mandir}/man1/sndfile-metadata-get.1*
%{_mandir}/man1/sndfile-metadata-set.1*
%{_mandir}/man1/sndfile-play.1*
%{_mandir}/man1/sndfile-salvage.1*

%changelog
* Mon Feb 03 2020 chenmaodong<chenmaodong@huawei.com> - 1.0.28-18
- Type:cves
- ID:CVE-2017-8362
- SUG:NA
- DESC:fix CVE-2017-8362

* Fri Jan 10 2020 chenmaodong<chenmaodong@huawei.com> - 1.0.28-17
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:use libgsm inside libsndfile

* Wed Jan 08 2020 chenmaodong<chenmaodong@huawei.com> - 1.0.28-16
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:remove sensitive information

* Mon Dec 23 2019 chenmaodong<chenmaodong@huawei.com> - 1.0.28-15
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:sync patches from next_kernel

* Fri Dec 20 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.0.28-14
- Fix ldconfig scriptlets

* Sat Apr 06 2019 luochunsheng<luochunsheng@huawei.com> - 1.0.28-13
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:remove sensitive information

* Thu Apr 04 2019 liuqianya<liuqianya@huawei.com> - 1.0.28-12
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix CVE-2018-19662

* Fri Mar 29 2019 zhangwenlong<zhangwenlong13@huawei.com> - 1.0.28-11
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix CVE-2019-3832 

* Wed Feb 13 2019 cangyi<cangyi@huawei.com> - 1.0.28-10
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:sync

* Mon Sep 10 2018 openEuler Buildteam <buildteam@openeuler.org> -1.0.28-9
- Package init
